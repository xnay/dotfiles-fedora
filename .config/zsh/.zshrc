# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

setopt autocd # Automatically cd into typed directory.

# History in cache directory:
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zsh/history

# Basic auto/tab complete:
autoload -U compinit && compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
_comp_options+=(globdots) # Include hidden files.

setopt AUTO_PUSHD        # Push the current directory visited on the stack.
setopt PUSHD_IGNORE_DUPS # Do not store duplicates in the stack.
setopt PUSHD_SILENT      # Do not print the directory stack after pushd or popd.

bindkey "^[[H" beginning-of-line
bindkey "^[[F" end-of-line
bindkey "^[[3~" delete-char
bindkey "^[[5~" beginning-of-history
bindkey "^[[6~" end-of-history

# Aliases:

# .zshrc
alias zrc='code ~/.config/zsh/.zshrc'

# Current project
alias cproj='cd /home/xnay/code/service-experimental'

# pacman
alias paci='sudo pacman -S'
alias pacu='sudo pacman -Syu && zinit self-update'
alias pacr='sudo pacman -Rs'
alias pacs='sudo pacman -Ss'

# dnf
alias diw='dnf --setopt=install_weak_deps=False --best install'
alias dI='dnf --best install'
alias dcl='dnf clean all'
alias dC='dnf check'
alias dU='dnf upgrade'
alias dS='dnf searche'
alias dar='dnf autoremove' # Removes packages installed as dependencies that are no longer required by currently installed programs.

# yay
alias yu='yay -Syu --combinedupgrade'
alias ys='yay -Ss'
alias yi='yay -S'
alias yr='yay -Rs'

# kubectl
alias k='kubectl'
alias kg='kubectl get'
alias kdel="kubectl delete $1 $2"

# Misc
alias prg='cd ~/code && ls'
alias mkdir='mkdir -pv'
alias l='ls -ghAFG --color=auto --group-directories-first'
alias ls='ls -ghAFG --color=auto --group-directories-first'
alias grep='grep -iF'

jsn() { 
    local str="${1}"
    echo "$str" | jq .
}

# Git
alias gc='git clone'

# Docker
alias di="docker images"
alias dia="docker images -a"
alias dps="docker ps" 
alias dirm="docker image remove"
alias dprune="docker system prune -f"
dstop() { docker stop $(docker ps -a -q); } # Stop all containers.
drall() { docker rm $(docker ps -a -q); } # Remove all containers.

# Youtubedl
alias ydlF='youtube-dl -F'
alias ydla='youtube-dl -x --audio-quality 0'


# Source nvm
source '/usr/share/nvm/init-nvm.sh'

# Zinit
# ----------------------------------------------------------------------------

### Added by Zinit's installer
if [[ ! -f $HOME/.zinit/bin/zinit.zsh ]]; then
    print -P "%F{33}▓▒░ %F{220}Installing %F{33}DHARMA%F{220} Initiative Plugin Manager (%F{33}zdharma/zinit%F{220})…%f"
    command mkdir -p "$HOME/.zinit" && command chmod g-rwX "$HOME/.zinit"
    command git clone https://github.com/zdharma/zinit "$HOME/.zinit/bin" &&
        print -P "%F{33}▓▒░ %F{34}Installation successful.%f%b" ||
        print -P "%F{160}▓▒░ The clone has failed.%f%b"
fi

source "$HOME/.zinit/bin/zinit.zsh"
autoload -Uz _zinit
((${+_comps})) && _comps[zinit]=_zinit

# Load a few important annexes, without Turbo
# (this is currently required for annexes)
zinit light-mode for \
    zinit-zsh/z-a-rust \
    zinit-zsh/z-a-as-monitor \
    zinit-zsh/z-a-patch-dl \
    zinit-zsh/z-a-bin-gem-node

### End of Zinit's installer chunk

zinit wait lucid light-mode for \
    atinit"zicompinit; zicdreplay" \
    zdharma/fast-syntax-highlighting \
    atload"_zsh_autosuggest_start" \
    zsh-users/zsh-autosuggestions \
    blockf atpull'zinit creinstall -q .' \
    zsh-users/zsh-completions

# Docker completion.
zinit ice as"completion"
zinit snippet https://github.com/docker/cli/blob/master/contrib/completion/zsh/_docker

zinit ice as"completion"
zinit snippet https://github.com/docker/compose/tree/master/contrib/completion/zsh/_docker-compose

# LSColors
zinit ice atclone"dircolors -b LS_COLORS > clrs.zsh" \
    atpull'%atclone' pick"clrs.zsh" nocompile'!' \
    atload'zstyle ":completion:*" list-colors “${(s.:.)LS_COLORS}”'
zinit light trapd00r/LS_COLORS

# Powerlevel 10k.
zinit ice depth=1
zinit light romkatv/powerlevel10k

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.config/zsh/.p10k.zsh ]] || source ~/.config/zsh/.p10k.zsh

autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /tmp/go-build188532560/b001/exe/main self

complete -o nospace -C /tmp/go-build469086590/b001/exe/main self
