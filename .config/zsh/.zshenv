#!/usr/bin/zsh

# Other XDG paths.
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}

# Fixing Paths.
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export ICEAUTHORITY="$XDG_CACHE_HOME"/ICEauthority
export ZDOTDIR=$HOME/.config/zsh
export HISTFILE="$XDG_CACHE_HOME"/zsh/history
export GOPATH="$HOME"/go
export GOBIN=$GOPATH/bin
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc
export GNUPGHOME="$XDG_DATA_HOME"/gnupg

export NODE_ENV=development

if [ -L /usr/lib/jvm/default ]; then
	export JAVA_HOME=/usr/lib/jvm/default
fi

# Path
typeset -U PATH path
path=("$HOME/go/bin" "$JAVA_HOME/bin" "$XDG_DATA_HOME/npm/bin" "$HOME/.local/bin" "$path[@]")
export PATH
